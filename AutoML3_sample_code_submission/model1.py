# -*- coding: utf-8 -*-

import pickle
import numpy as np
from os.path import isfile
import random
from sklearn.externals import joblib
import pandas as pd
import data_io
from preprocessing import FeatureProcessing
import os
# from sklearn.cross_validation import cross_val_predict

os.system('apt-get update')
os.system('apt-get upgrade')
os.system('apt-get dist-upgrade')
os.system('apt-get install libstdc++6')
os.system('apt-get install cmake -y')
os.system('apt-get install build-essential -y')
os.system("pip install lightgbm")
os.system('mkdir models')

import lightgbm as lgb


class Model:
    lgb_isinstalled = False
    dataset = 0

    def __init__(self):

        if Model.lgb_isinstalled is False:
            Model.lgb_isinstalled = True
        self.feature_type, self.datasetname = self.read_info_return_list(Model.dataset)
        self.early_stopping = 45
        self.num_train_samples = 0
        self.num_feat = 1
        self.num_labels = 1
        self.is_trained = False
        self.clf = None
        self.params = {'task': 'train', 'boosting_type': 'gbdt', 'objective': 'binary', 'metric': 'auc',
                       'learning_rate': 0.01, 'num_leaves': 50, 'num_iterations': 2000, 'verbose': 0,
                       'colsample_bytree': 0.6, 'subsample': 0.8, 'max_depth': 7, 'reg_alpha': 0.5, 'reg_lambda': 0.5,
                       'min_split_gain': 0.05, 'min_child_weight': 25}
        self.X = None
        self.y = None
        self.fp = FeatureProcessing(self.feature_type)
        Model.dataset = Model.dataset + 1
        self.cate_feat = []

        self.batchnumber = 0

    def fit(self, X, y):
        if X.shape[0] > 250000:
            X, y = self.resample(X, y, rownum=250000)
        self.params['learning_rate'] = self.params['learning_rate'] + 0.01 * self.batchnumber
        X, _ = self.fp.fit(X)

        if not self.is_trained:
            self.X = X
            self.y = y
        else:
            self.X = np.concatenate([self.X, X], axis=0)
            self.y = np.concatenate([self.y, y], axis=0)
        # print "the shape is ", self.X.shape, self.y.shape

        print 'self.X', self.X[0:2, :], 'self.y', self.y

        X_train, X_val, y_train, y_val = train_test_split(self.X, self.y, test_size=0.03, random_state=2018)
        train_data = lgb.Dataset(data=X_train, label=y_train.reshape(-1, ), free_raw_data=False)
        val_data = lgb.Dataset(data=X_val, label=y_val.reshape(-1, ), free_raw_data=False)

        self.clf = lgb.train(
            self.params, train_data,
            valid_sets=[train_data, val_data], early_stopping_rounds=45, verbose_eval=100
        )
        self.bast_iteration = self.clf.best_iteration
        self.is_trained = True
        self.batchnumber += 1

    def predict(self, X):
        X, _ = self.fp.fit(X)
        y_combine = self.clf.predict(X)
        return y_combine

    def save(self, path="./"):
        pickle.dump(self, open(path + '_model.pickle', "w"))

    def load(self, path="./"):
        modelfile = path + '_model.pickle'
        if isfile(modelfile):
            with open(modelfile) as f:
                self = pickle.load(f)
            print("Model reloaded from: " + modelfile)
        return self

    def resample(self, X, y, rownum=None):
        dfx = pd.DataFrame(X)
        dfy = pd.DataFrame(y)
        dfxs = dfx.sample(n=rownum)
        idx = dfxs.index.values
        dfys = dfy.ix[idx]
        return dfxs.values, dfys.values

    def read_info_return_list(self, count_number):
        from sys import argv
        root_dir = os.path.dirname(os.path.realpath(__file__))
        print 'root_dir', root_dir
        if len(argv) == 1:
            input_dir = os.path.join(root_dir, "AutoML3_sample_data")
        else:
            input_dir = os.path.abspath(argv[1])
        print 'input_dir', input_dir
        datanames = data_io.inventory_data(input_dir)

        if (os.name == "nt"):
            filesep = '\\'
        else:
            filesep = '/'

        original_type = []
        feature_type = []
        path = input_dir + '/' + datanames[count_number] + '/' + datanames[count_number] + "_feat.type"
        print 'read_info_return_list path', path
        with open(path, "r") as typefile:
            for line in typefile:
                if line.strip("\n").strip() == 'Numerical':
                    original_type.append(0)
                elif line.strip("\n").strip() == 'Categorical':
                    original_type.append(1)
                elif line.strip("\n").strip() == 'Time':
                    original_type.append(2)
                elif line.strip("\n").strip() == 'Multi-value':
                    original_type.append(3)
        num_of_numerical = original_type.count(0)
        num_of_categorical = original_type.count(1)
        num_of_time = original_type.count(2)
        num_of_multicate = original_type.count(3)
        for i in range(num_of_time):
            feature_type.append(2)
        for i in range(num_of_numerical):
            feature_type.append(0)
        for i in range(num_of_categorical):
            feature_type.append(1)
        for i in range(num_of_multicate):
            feature_type.append(3)
        print feature_type
        return feature_type, datanames[count_number]
