import numpy as np
from sklearn.feature_selection import RFE, SelectFromModel, RFECV
from sklearn.linear_model import LogisticRegression, Lasso, Ridge, BayesianRidge, SGDRegressor
import pandas as pd
from sklearn.decomposition import PCA


class FeatureProcessing():
    def __init__(self, feature_type):
        self.target_type = 1
        self.feature_type = feature_type

    def category_feat(self, dfcat):
        for col in dfcat:
            gb = dfcat.groupby(col).size().reset_index()
            gb[0] = gb[0] / dfcat.shape[0]
            gb.rename(columns={col: str(col) + 'gb', 0: str(col) + '_cnt'}, inplace=True)
            dfcat = pd.merge(dfcat, gb, left_on=col, right_on=str(col) + 'gb', how='left')
            del dfcat[col]
            del dfcat[str(col) + 'gb']
        return dfcat

    def bi_category_feat(self, catedf):
        cols = [x for x in catedf.columns]
        print 'bi_category_feat:', cols
        for i in range(2, 5):
            for j in range(i + 1, len(cols)):
                col1 = cols[i]
                col2 = cols[j]
                gb = catedf.groupby([col1, col2]).size().reset_index()
                gb[0] = gb[0] / catedf.shape[0]
                gb.rename(columns={0: str(col1) + str(col2) + '_cnt'}, inplace=True)
                catedf = pd.merge(catedf, gb, on=[col1, col2], how='left')
                print 'catedf',catedf.head()

        for x in cols:
            del catedf[x]
        return catedf

    def get_pos_num(self, Xtr, Xte, Ytr, Yte):
        Ytr = pd.DataFrame(data=Ytr, columns=['label'])
        Yte = pd.DataFrame(data=Yte, columns=['label'])
        dtr1 = pd.DataFrame(data=Xtr, dtype=np.string_)
        dte1 = pd.DataFrame(data=Xte, dtype=np.string_)
        dtr = pd.concat([Ytr, dtr1], axis=1)
        dte = pd.concat([Yte, dte1], axis=1)

    def get_pvfeat(self, Xtr, Xte, Ytr, Yte):
        # print "self.all_cate_features", self.all_cate_features
        # Xtr1 = Xtr[self.all_cate_features]
        # Xte1 = Xte[self.all_cate_features]
        Ytr = pd.DataFrame(data=Ytr, columns=['label'])
        Yte = pd.DataFrame(data=Yte, columns=['label'])
        dtr1 = pd.DataFrame(data=Xtr, dtype=np.string_)
        dte1 = pd.DataFrame(data=Xte, dtype=np.string_)

        print 'Ytr', Ytr, 'Yte', Yte
        dtr = pd.concat([Ytr, dtr1], axis=1)
        print 'dtr', dtr.head()
        dtr = dtr.reset_index()
        print 'dtr', dtr.head()
        dte = pd.concat([Yte, dte1], axis=1)
        print 'dte', dte.head()
        dte = dte.reset_index()
        print 'dte', dte.head()
        for col in [45, 46, 47, 48]:
            gb = dtr.groupby(col).agg({col: np.size, 'label': np.sum})
            gb.rename(columns={col: 'idx', 'label': str(col) + 'label'}, inplace=True)
            gb = gb.reset_index()
            gb[str(col) + 'ctr'] = gb[str(col) + 'label'] / (gb['idx'] + 10)
            dtrgb = gb[[col, str(col) + 'ctr']]
            dte = pd.merge(dte, dtrgb, on=col, how='left')
            dte.fillna(0, inplace=True)

            # del dte['index']
            print 'for dte', dte.tail(), 'shape', dte.shape
            gb = dte.groupby(col).agg({col: np.size, 'label': np.sum})
            gb.rename(columns={col: 'idx', 'label': str(col) + 'label'}, inplace=True)
            gb = gb.reset_index()
            gb[str(col) + 'ctr'] = gb[str(col) + 'label'] / (gb['idx'] + 10)
            dtegb = gb[[col, str(col) + 'ctr']]
            # print 'for dtegb', dtegb.head()
            dtr = pd.merge(dtr, dtegb, on=col, how='left')
            dtr.fillna(0, inplace=True)
            del dte[col]
            del dtr[col]
            # del dtr['index']
            print 'for dtr', dtr.head(), 'shape', dtr.shape

        dtr.sort_values(['index'], ascending=True, inplace=True)
        dte.sort_values(['index'], ascending=True, inplace=True)
        print 'dtr after ', dtr.head(), 'dte after', dte.head()
        Xtr = dtr.values[:, 2:]
        Xte = dte.values[:, 2:]
        Ytr = dtr.values[:, 1]
        Yte = dte.values[:, 1]
        return Xtr, Xte, Ytr, Yte

    def time_feature_process(self, df):
        df = df.astype(dtype=np.float64)
        for col in df.columns:
            min = df[col].min()
            df[col] = (df[col] - min)
        return df

    def continues_feat(self, num_feature):
        '''
            delete the outlier data
        '''
        num_feature = num_feature.astype(np.float64)
        print "denosing number type features.......", num_feature.head()
        for col in num_feature.columns:
            std = num_feature[col].std()
            mean = num_feature[col].mean()
            num_feature[col] = (num_feature[col] - mean) / (std + 0.00001)
            num_feature[col] = np.where(num_feature[col] > 3, 3, num_feature[col])
            num_feature[col] = np.where(num_feature[col] < -3, -3, num_feature[col])
        # print 'continues_feat',num_feature.head()
        return num_feature

    def feature_selection(self, clf_feat, num_feat, target, classification_cols, continuous_cols):
        print "doing feature selection.........."
        train_data = pd.concat([clf_feat, num_feat], axis=1, ignore_index=False)
        selector = RFECV(estimator=BayesianRidge())
        selector.fit(train_data.values, target.values.ravel())
        try:
            support = selector.support_
        except:
            support = selector.get_support()

        new_df_selected = train_data.T.iloc[support].T
        selected_cols = new_df_selected.columns.values
        selected_classification_cols = list(set(selected_cols).intersection(set(classification_cols)))
        selected_continuous_cols = list(set(selected_cols).intersection(set(continuous_cols)))
        selected_classification_cols = selected_classification_cols
        selected_continuous_cols = selected_continuous_cols
        return abs(new_df_selected[selected_classification_cols]), abs(new_df_selected[selected_continuous_cols])

    def _pca(self, train_data):
        if self.using_pca is False:
            return np.array(train_data)
        else:
            pca = PCA(n_components=0.95)
            X = pca.fit_transform(train_data)
        return X

    def hash100000(self, x):
        return abs(hash(x) % 300000)

    def multicate(self, df):
        print 'multi cate df', df.head()
        tmp = []
        for col in df.columns:
            tdf = df[col].str.split(',', expand=True)
            for cl in tdf.columns:
                tdf.rename(columns={cl: str(col)+'multi' + str(cl)}, inplace=True)
            tmp.append(tdf)
        df = pd.concat(tmp, axis=1)
        df = df.applymap(self.hash100000)

        print 'multi cate df *****', df.head()
        return df

    def fit(self, train_data):
        train_data = pd.DataFrame(train_data)
        self.train_data = train_data
        self.df_index = train_data.index
        self.columns = train_data.columns
        self.numerical_cols = train_data.columns[np.where(np.array(self.feature_type) == 0)[0]]
        print 'self.numerical_cols', self.numerical_cols
        self.categorical_cols = train_data.columns[np.where(np.array(self.feature_type) == 1)[0]]
        print 'self.categorical_cols', self.categorical_cols
        self.time_series_cols = train_data.columns[np.where(np.array(self.feature_type) == 2)[0]]
        print 'self.time_series_cols', self.time_series_cols
        self.multi_value_cols = train_data.columns[np.where(np.array(self.feature_type) == 3)[0]]
        print 'self.multi_value_cols', self.multi_value_cols

        cate_features = train_data[self.categorical_cols]
        print 'cate_features before', cate_features.head()
        cate_features_pv = self.category_feat(cate_features)
        print 'cate_features after', cate_features.head()

        num_features = train_data[self.numerical_cols]
        # print 'num_features before', num_features.head()
        # num_features = self.continues_feat(num_features)
        # print 'num_features after', num_features.head()

        time_features = train_data[self.time_series_cols]
        time_features = self.time_feature_process(time_features)

        multi_cate_features = train_data[self.multi_value_cols]
        multi_cate_features = self.multicate(multi_cate_features)
        multi_cate_features_pv = self.category_feat(multi_cate_features)
        data_processe_cont = pd.concat([time_features, num_features, cate_features_pv, multi_cate_features_pv], axis=1)
        data_processe_cate = pd.concat([cate_features, multi_cate_features], axis=1)
        bi_category_feat = self.bi_category_feat(data_processe_cate)
        print 'bi_category_feat after ', bi_category_feat.head()
        data_processe_cont = pd.concat([data_processe_cont, bi_category_feat], axis=1)
        print 'num_features', num_features.head(), ' shape: ', num_features.shape
        print 'cate_features', cate_features.head(), ' shape: ', cate_features.shape
        # print 'multi_cate_features', multi_cate_features.head(), ' shape: ', multi_cate_features.shape
        print 'data_processe value', data_processe_cont.ix[0]
        return np.array(data_processe_cont.values, dtype=np.float64), np.array(data_processe_cate.values,
                                                                               dtype=np.float64)

